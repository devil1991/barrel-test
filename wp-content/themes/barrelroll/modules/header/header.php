<header class="header z8 container container--xxl container--nogutter" data-module="header">
  <div class="header__notice align-c"><span>Sponsored By</span></div>
  <div class="header__main">
    <a href="<?php home_url('/'); ?>" class="header__main__el header__main__el--brand">
      <img src="<?php echo get_image_with_size(get_field('brand_logo_image', 'option'), 'tiny'); ?>" alt="">
    </a>
    <div class="header__main__el header__main__el--logo">
      <a href="<?php home_url('/'); ?>">
        <img src="<?php echo get_image_with_size(get_field('header_logo_image', 'option'), 'tiny'); ?>" alt="<?php bloginfo( 'name' ); ?>">
      </a>
    </div>
    <div class="header__main__el header__main__el--share">
      <?php the_module('social-icons'); ?>
    </div>
  </div>
</header>
