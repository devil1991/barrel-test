/**
* Initializes the site's footer module.
* @constructor
* @param {Object} el - The site's footer container element.
*/
function footer (el) {
  this.el = el
}

export default footer
