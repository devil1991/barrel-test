<footer class="footer">
  <div class="footer__wrapper">
    <div class="footer__col footer__col--share">
      <span class='tan bold uppercase small'>Share On:</span> <?php the_module('social-icons'); ?>
    </div>
    <div class="footer__col footer__col--logo">
      <img src="<?php echo get_image_with_size(get_field('brand_logo_image', 'option'), 'tiny'); ?>" alt="">
    </div>
  </div>
</footer>
