/**
* Initializes the site's bottleintro module.
* @constructor
* @param {Object} el - The site's bottleintro container element.
*/
import { addClass } from 'lib/dom'
import imagesloaded from 'imagesloaded'

function bottleintro (el) {
  this.el = el
  imagesloaded(el, () => {
    addClass('loaded', this.el)
  })
}

export default bottleintro
