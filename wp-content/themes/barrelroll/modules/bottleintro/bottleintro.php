<section class="bottleintro container container--xxl container--nogutter" data-module="bottleintro">
  <div class="bottleintro__wrapper relative">
    <div class="bottleintro__bg" style="background-image: url(<?php echo get_image_with_size( $backgound_image ,'large');?>);"></div>
    <div class="bottleintro__container f fw">
      <div class="bottleintro__text z1 white">
        <h2 class="title type--reset"><?php echo $title;?></h2>
        <p><?php echo $introduction;?></p>
      </div>
      <div class="bottleintro__bottle z1">
        <img src="<?php ;echo get_image_with_size( $bottle_image ,'large') ?>" alt="Bottle Image">
      </div>
    </div>
  </div>
</section>