<?php
  $articlesQuery = new WP_Query(array(
    'posts_per_page' => isset($count) ? $count : 9,
    'post__not_in'   => isset($exclude_ids) ? $exclude_ids : array(),
    'post_status'    => 'publish',
    'post_type'      => 'post',
    'paged'          =>  isset($paged) ? $paged : 1
  ));
  
  if ($articlesQuery->have_posts()):
?>
  <section class="recent-articles container container--xxl container--nogutter" data-module="recent-articles" data-currentpage='<?php echo $articlesQuery->query_vars['paged']; ?>' data-maxpage='<?php echo $articlesQuery->max_num_pages; ?>'>
    <div class="container">
      <h3 class="recent-articles__title heading align-c"><?php _e('Recent Articles', Base_Theme::$text_domain);?></h3>
      <div class="recent-articles__articles f fw">
        <?php while ( $articlesQuery->have_posts() ) : $articlesQuery->the_post(); ?>
          <?php the_module('article-box'); ?>
        <?php endwhile; wp_reset_postdata(); ?>
      </div>
    </div>
  </section>
<?php endif; ?>