/**
* Initializes the site's recentarticles module.
* @constructor
* @param {Object} el - The site's recentarticles container element.
*/
import ScrollWatch from 'scrollwatch'
import request from 'axios'
import TweenMax from 'gsap/TweenMax'
async function loadPosts (page = 1) {
  const { data } = await request.get('/', { params: { page } })
  const container = document.createElement('div')
  container.innerHTML = data
  return container.querySelectorAll('.recent-articles .article-box')
}

function getPagination (el) {
  return {
    currentPage: parseInt(el.getAttribute('data-currentpage')),
    maxpage: parseInt(el.getAttribute('data-maxpage'))
  }
}

class Recentarticles {
  constructor (el) {
    this.el = el
    this.data = getPagination(el)
    this.loading = false
    this.handlePagination = this.handlePagination.bind(this)
    if (this.data.currentPage === this.data.maxpage) return this

    this.articlesWrap = el.querySelector('.recent-articles__articles')
    this.scrollWatch = new ScrollWatch({
      infiniteScroll: true,
      infiniteOffset: 400,
      watch: '.article-box',
      inViewClass: 'inView',
      onInfiniteYInView: this.handlePagination
    })
  }

  async handlePagination ({ direction }) {
    if (this.loading || direction !== 'down') return
    this.scrollWatch.pauseInfiniteScroll()
    this.loading = true
    const posts = await loadPosts(this.data.currentPage + 1)
    this.data.currentPage += 1
    TweenMax.set(posts, { opacity: 0, y: 30 })
    this.articlesWrap.append(...posts)
    TweenMax.staggerTo(posts, 0.6, { opacity: 1, y: 0 }, 0.2)
    if (this.data.currentPage === this.data.maxpage) return
    this.loading = false
    this.scrollWatch.resumeInfiniteScroll()
    this.scrollWatch.refresh()
  }
}

function recentarticles (el) {
  this.controller = new Recentarticles(el)
}

export default recentarticles
