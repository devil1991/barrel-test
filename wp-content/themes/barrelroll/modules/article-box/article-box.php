<?php 
  $article_kind = get_field('kind');
  $is_highlight = isset($highlight) ? $highlight : false;
?>
<div class="article-box <?php echo $is_highlight ? 'article-box--highlight container container--xxl container--nogutter' : '';?>" href="<?php the_permalink(); ?>" data-kind="<?php echo $article_kind; ?>" data-id="<?php the_id(); ?>">
  <div class="article-box__wrap relative f fdc">
    <div class="article-box__image relative" style="background-image: url(<?php echo get_thumbnail_url_with_size('medium');?>);">
    </div>
    <div class="article-box__content relative align-c">
      <div class="article-box__icon f aic jcc z1">
        <?= _get_svg($article_kind); ?>
      </div>
      <div class="article-box__date uppercase"><?php the_date('F d'); ?></div>
      <h5 class="subheading--s "><?php the_title(); ?></h5>
      <?php if ($is_highlight): ?>
        <div class='article-box__excerpt'><?php echo the_truncated_excerpt(get_the_excerpt(), 200); ?></div>
      <?php endif;?>
      <a class='article-box__cta absolute align-c type--reset bold decoration--none' href="<?php the_permalink(); ?>">
        <?php switch ($article_kind) {
          case 'gallery':
            _e('View Gallery', Base_Theme::$text_domain);
            break;
          case 'video':
            _e('Watch Video', Base_Theme::$text_domain);
            break;
          default:
            _e('Read More', Base_Theme::$text_domain);
            break;
        }?>
      </a>
    </div>
  </div>
</div>