<?php

add_filter('the_content', function( $content ){
  if ( is_singular('event') ) {
    global $post;
    $contents = explode( "<!--more-->", $post->post_content );

    if ( count( $contents ) > 1 ) {
      $content = sprintf("<p>") . wpautop($contents[0]) . sprintf(" <a href=\"#\" class=\"js-more-show\">Read More</a></p><div class=\"more-content\">%s</div>", wpautop($contents[1]) . sprintf("<p><a href=\"#\" class=\"js-more-hide\">Show Less</a></p>"));
    }
  }

  return $content;
});

add_action('rest_api_init', function () {
  function get_post_kind ($object, $field_name, $request) {
    return get_field('kind', $object[ 'id' ]);
  }

  function get_post_metas ($object, $field_name, $request) {
    return (array) get_fields($object['ID']);
  }

  register_rest_field('post',
    'kind',
    array(
        'get_callback'    => 'get_post_kind',
        'update_callback' => null,
        'schema'          => 'string',
      )
    );

  register_rest_field('post',
    'meta',
    array(
        'get_callback'    => 'get_post_metas',
        'update_callback' => null,
        'schema'          => null,
      )
    );
}, 10);