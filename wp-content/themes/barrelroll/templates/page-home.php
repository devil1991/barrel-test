<?php
  /*
  Template Name: Homepage
  Template Post Type: page
  */
  global $post;
  get_header();

  $highlight_post = get_field('highlighted_post');

  // Exclude highlighted post from recent-articles grid
  $exclude_ids = array();
  if ($highlight_post) {
    $exclude_ids[] = $highlight_post->ID;
  }

  the_module('bottleintro', array(
    'title'            => get_field('intro_title'),
    'introduction'     => get_field('introduction'),
    'bottle_image'     => get_field('bottle_image'),
    'backgound_image'  => get_field('background_image')
  ));

  if ($highlight_post) {
    $post = $highlight_post;
    setup_postdata($post);
    the_module('article-box', array(
      'highlight' => true
    ));
    wp_reset_postdata();
  }

  the_module('recent-articles', array(
    'exclude_ids' => $exclude_ids, // add the post selected as highlught on home
    'count'       => 2,
    'paged'       => get_query_var('page', 1)
  ));

  get_footer();
?>
